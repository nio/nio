#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(defpackage :nio-httpd (:use :cl :nio-server :nio) 
	    (:export start))
(in-package :nio-httpd)

(defun method-sig (method)
  (logior (ash (logand (aref method 0) #xDF) 24)
	  (ash (logand (aref method 1) #xDF) 16)
	  (ash (logand (aref method 2) #xDF) 8)
	  (ash (logand (aref method 3) #xDF) 0)))

(defun map-method (method)
  (case (method-sig method)
    (#x47455400 :get)
    (#x504F5354 :post)
    (#x48454144 :head)
    (t :unknown)))

(defun http-split-req (data)
  (let* ((line-end (position 13 data))
	 (line (subseq data 0 line-end))
	 (sp1 (position #.(char-code #\space) line))
	 (sp2 (position #.(char-code #\space) line :start (1+ sp1)))
	 (req (subseq line 0 (1+ sp1)))
	 (url (subseq line (1+ sp1) sp2))
	 (proto (subseq line (1+ sp2))))

    (list (map-method req)
	  url
	  proto
	  (+ line-end 2))))

(defparameter +page-hash+ (make-hash-table :test #'equalp))

(defun string-to-octets (string)
  (map 'vector #'char-code string))



(defparameter +dyn-pre+ (string-to-octets "<html><body>"))
(defparameter +dyn-post+ (string-to-octets "<h5>Page created by NIO-HTTPD.</h5></body></html>"))

(defvar +pages-served+ 0)

(defun dynamic-gen (method url data hdrs-start client)
  (declare (ignore method url data hdrs-start client))
  (concatenate 'vector
	       +dyn-pre+
	       
	       (string-to-octets
		(with-output-to-string (str)
		  (princ "<h2>Hello stranger!</h2>" str)
		  
		  (princ "Today is " str)
		  (multiple-value-bind (sec min hrs day mon yrs) (decode-universal-time (get-universal-time))
		    (declare (ignore sec min hrs))
		    (loop for elt in (list day "." mon "." yrs) do (princ elt str)))

		  (princ "<br/>" str)

		  (princ "I have now served " str)
		  (princ (incf +pages-served+) str)
		  (princ " pages.<br/>" str)

		  str))

	       +dyn-post+))


(defun start ()

  (setf (gethash (string-to-octets "/") +page-hash+) 
	(string-to-octets "<html><body>
<h2>Hello, world!</h2>This is NIO-HTTPD v0.001. Powered by Alien Technology..<br/>
</body></html>"))

  (setf (gethash (string-to-octets "/dyn") +page-hash+)
	#'dynamic-gen)


  (let ((headers200 
	 (concatenate 'vector 
		      (string-to-octets "HTTP/1.0 200 OK")
		      #(13 10)
		      (string-to-octets "Content-type: text/html")
		      #(13 10)))

	(page404
	 (concatenate 'vector
		      (string-to-octets "HTTP/1.0 404 Not found")
		      #(13 10)
		      (string-to-octets "Content-type: text/html")
		      #(13 10)
		      #(13 10)
		      (string-to-octets "<html><body>The requested document was not found. Sorry.</body></html>")
		      ))

	(method-not-implemented
	 (concatenate 'vector
		      (string-to-octets "HTTP/1.0 501 Method Not Implemented")
		      #(13 10 13 10)))
	)


  (start-server 
   #'(lambda (client data)
       (destructuring-bind (req url proto hdrs-start) (http-split-req data)
	 (declare (ignore proto))
	 (case req

	   (:get 
	    (let ((page (gethash url +page-hash+)))
;;	      (format t "Page for ~S is ~S.~%" url page)
	      (if page
		  (progn
		    (write-socket client headers200)
		    (let ((content 
			   (if (vectorp page)
			       page
			       (funcall page :get url data hdrs-start client))))
		      (write-socket client 
				    (concatenate 'vector
						 (string-to-octets
						  (with-output-to-string (str)
						    (princ "Content-length: " str)
						    (princ (length content) str)
						    str))
						 #(13 10 13 10)
						 content))))
		  
		  (write-socket client page404))))

	   (:head (write-socket client method-not-implemented))
	   (:post (write-socket client method-not-implemented))
	   (t (write-socket client method-not-implemented))
       )))

   #'(lambda (x) (search #(13 10 13 10) x))

   :host "127.0.0.1"
   )

  ))
   
