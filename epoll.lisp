#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :event-notification)

#+linux
(progn

  (defstruct epoll-event-queue fd)

  (defun make-event-queue ()
    (make-epoll-event-queue :fd (%epoll-create 10)))


  (defun add-fd (event-queue fd &key (mode '(:read :write)) (trigger :edge))
    (with-foreign-object (ev 'epoll-event)
      (memzero ev +epoll-event-size+)

      (setf (foreign-slot-value ev 'epoll-event 'fd)  fd

	    (foreign-slot-value ev 'epoll-event 'events)
	    (logior (if (member :read mode) +epoll-in+ 0) 
		    (if (member :write mode) +epoll-out+ 0) 
		    (if (eql trigger :edge) +epoll-et+)))

      (%epoll-ctl (epoll-event-queue-fd event-queue) +epoll-ctl-add+ fd ev)))


  (defun remove-fd (event-queue fd)
    (with-foreign-object (ev 'epoll-event)
      (memzero ev +epoll-event-size+)

      (setf (foreign-slot-value ev 'epoll-event 'fd) fd)

      (%epoll-ctl (epoll-event-queue-fd event-queue) +epoll-ctl-del+ fd ev)))

  (defun poll-events (event-queue)
    (with-foreign-object (events 'epoll-event 10)
      (memzero events (* +epoll-event-size+ 10))
      (loop for res = (%epoll-wait (epoll-event-queue-fd event-queue) events 10 -1)
	   do
	   (case res
	     (-1 
	      ;;	    (%warn "Error in poll-kevent")
	      (return nil)) ;; FIXME should maybe check error code ?
	     (0 nil)
	     (t 
	      (let ((idents nil))
		(loop for i from 0 below res do
		     (push (foreign-slot-value 
			    (mem-aref events 'epoll-event i) 
			    'epoll-event 'fd)
			   idents))
		(return idents)))))))


  )
