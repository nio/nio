#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :event-notification)

#+linux
(progn

  (defcfun ("warn" %warn) :void
    (format :string))

  (defcfun ("memset" %memset) :pointer
    (buffer :pointer)
    (byte :int)
    (len :int))

  (defun memzero (ptr size)
    (%memset ptr 0 size))

  (defctype :uint32 :unsigned-long)
#|
  (defcunion epoll-data
      (ptr :pointer)
    (fd :int)
    (u32 :uint32)
    (u64 :uint32 :count 2)) 

  (defcstruct epoll-event
      (events :uint32)
    (data epoll-data))
|#
  (defcstruct epoll-event
      (events :uint32)
    (fd :int)
    (pad :uint32))

  (defconstant +epoll-event-size+ #.(+ 4 4 4))

  (defconstant +epoll-in+ #x001)
  (defconstant +epoll-out+ #x004)
  (defconstant +epoll-et+ #.(ash 1 31)) 

  (defconstant +epoll-ctl-add+ 1)
  (defconstant +epoll-ctl-del+ 2)
  (defconstant +epoll-ctl-mod+ 3)

  (defcfun ("epoll_ctl" %epoll-ctl) :int
    (epoll-fd :int)
    (op :int)
    (fd :int)
    (event :pointer))

  (defcfun ("epoll_create" %epoll-create) :int
    (size-hint :int))

  (defcfun ("epoll_wait" %epoll-wait) :int
    (epoll-fd :int)
    (events :pointer)
    (maxevents :int)
    (timeout :int))

  )
