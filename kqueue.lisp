#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :event-notification)

#+(or darwin freebsd)
(progn
  (defstruct kqueue-event-queue kqueue)


  (defun make-event-queue ()
    (make-kqueue-event-queue :kqueue (%kqueue)))

  (defun flags (mode)
    (logior (if (member :read mode) +evfilt-read+ 0) 
	    (if (member :write mode) +evfilt-write+ 0)))

  (defun ev-set (kevent ident filter flags fflags data udata)
    (setf (foreign-slot-value kevent 'kevent 'ident) ident
	  (foreign-slot-value kevent 'kevent 'filter) filter
	  (foreign-slot-value kevent 'kevent 'flags) flags
	  (foreign-slot-value kevent 'kevent 'fflags) fflags
	  (foreign-slot-value kevent 'kevent 'data) data
	  (foreign-slot-value kevent 'kevent 'udata) udata))


  (defun add-fd (event-queue fd &key (mode '(:read :write)) (trigger :edge))
    (with-foreign-object (ke 'kevent)
      (ev-set ke fd 
	      (flags mode) 
	      (logior +ev-add+ (if (eql :edge trigger) +ev-clear+ 0)) 
	      0 0 (null-pointer))
      (%kevent (kqueue-event-queue-kqueue event-queue) ke 1 (null-pointer) 0 (null-pointer)))
    )

  (defun remove-fd (event-queue fd &key (mode '(:read :write)))
    (with-foreign-object (ke 'kevent)
      (ev-set ke fd (flags mode) +ev-delete+ 0 0 (null-pointer))
      (%kevent (kqueue-event-queue-kqueue event-queue) ke 1 (null-pointer) 0 (null-pointer))))


  (defun poll-events (event-queue)

    (with-foreign-object (ke 'kevent 10)
      (memzero ke (* +kevent-size+ 10))
      (loop for res = (%kevent (kqueue-event-queue-kqueue event-queue) (null-pointer) 0 ke 10 (null-pointer)) do
	   (case res
	     (-1 
	      ;;	    (%warn "Error in poll-kevent")
	      (return nil)) ;; FIXME should maybe check error code ?
	     (0 nil)
	     (t 
	      (let ((idents nil))
		(loop for i from 0 below res do
		     (push (foreign-slot-value (mem-aref ke 'kevent i) 'kevent 'ident) idents))
		(return idents)))))))

  )