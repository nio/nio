#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :nio)


;;;; CONSTANTS

(defconstant +af-inet+ 2)
(defconstant +af-inet6+ 30)

(defconstant +sock-stream+ 1)
(defconstant +sock-dgram+ 2)

(defconstant +cmd-get-flags+ 3)
(defconstant +cmd-set-flags+ 4)
(defconstant +arg-nonblock+ #x0004)


;;;; TYPES

(defctype :uint8 :unsigned-char)
(defctype :uint16 :unsigned-short)
(defctype :uint32 :unsigned-long)


;;;; GENERIC FUNCTIONS

(defcfun ("warn" %warn) :void
  (format :string))

(defcfun ("memset" %memset) :pointer
  (buffer :pointer)
  (byte :int)
  (len :int))

(defun memzero (ptr size)
  (%memset ptr 0 size))

(defun hexdump-foreign (obj size)
  (loop for i from 0 below size
     for xx = (mem-aref obj :unsigned-char i) do
       (format t "~2,'0X " xx)
     finally (terpri)))

(defun map-to-lisp-array (data bytes)
  (let ((arr (make-array bytes :element-type '(unsigned-byte 8) :adjustable nil :initial-element 0)))
    (loop for i from 0 below bytes 
	 for foreign-elt = (mem-aref data :uint8 i)
	 do (setf (aref arr i) foreign-elt))
    arr))

(defun map-to-foreign-array (data)
  (let ((arr (foreign-alloc :uint8 :count (length data))))
    (loop for i from 0 below (length data) 
	 for data-elt = (aref data i)
	 do (setf (mem-aref arr :uint8 i) data-elt))
    (list arr (length data))))


;;;; SOCKET FUNCTIONS / STRUCTURES

(defcstruct sockaddr-in
  (len :uint8)
  (family :uint8)
  (port :uint16)
  (addr :uint32)
  (zero :char :count 8))

(defconstant +sockaddr-in-len+ #.(+ 1 1 2 4 8))


(defcstruct sockaddr-in6
  (len :uint8)
  (family :uint8)
  (port :uint16)
  (flowinfo :uint32)
  (addr :uint16 :count 8)
  (scope-id :uint32))

(defconstant +sockaddr-in6-len+ #.(+ 1 1 2 4 16 4))

(defcfun ("socket" %socket) :int
  (domain :int)
  (type :int)
  (protocol :int))

(defcfun ("inet_pton" %inet-pton) :int
  (af :int)
  (src :string)
  (dst :pointer))

(defcfun ("htons" %htons) :uint16
  (host-value :uint16))

(defcfun ("bind" %bind) :int
  (socket :int)
  (sockaddr :pointer)
  (socklent :long))

(defcfun ("listen" %listen) :int
  (socket :int)
  (backlog :int))

(defcfun ("close" %close) :int
  (fd :int))

(defcfun ("accept" %accept) :int
  (socket :int)
  (sockaddr :pointer)
  (socklen :pointer))

(defcfun ("fcntl" %fcntl) :int
  (fd :int)
  (cmd :int)
  (arg :int))

(defcfun ("read" %read) :long
  (fd :int)
  (buffer :pointer)
  (nbytes :unsigned-long))

(defcfun ("write" %write) :long
  (fd :int)
  (buffer :pointer)
  (nbytes :unsigned-long))
