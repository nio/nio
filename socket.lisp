#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :nio)

(declaim (optimize (debug 3)))


(defun set-socket-nonblocking (socket-fd)
  (%fcntl socket-fd +cmd-set-flags+ +arg-nonblock+))

(defun start-listen (socket-fd &optional (backlog 3))
  (%listen socket-fd backlog))

(defun close-socket (socket-fd)
  (%close socket-fd))



;;;; IPv4

(defun make-inet-socket ()
  (%socket +af-inet+ +sock-stream+ 0))

(defun bind-inet-socket (socket-fd port &optional (addr "127.0.0.1"))
  (with-foreign-object (sa 'sockaddr-in)

    (memzero sa +sockaddr-in-len+)

    ;; init struct
    (setf (foreign-slot-value sa 'sockaddr-in 'len) +sockaddr-in-len+
	  (foreign-slot-value sa 'sockaddr-in 'port) (%htons port)
	  (foreign-slot-value sa 'sockaddr-in 'family) +af-inet+)

    ;; set addr
    (if (/= (%inet-pton +af-inet+ addr (foreign-slot-pointer sa 'sockaddr-in 'addr)) 1)
	(error "inet_pton: Bad address ~A!" addr))

    ;; bind
    (if (= (%bind socket-fd sa +sockaddr-in-len+) 0)
	t
	nil)))


;;;; IPv6

(defun make-inet6-socket ()
  (%socket +af-inet6+ +sock-stream+ 0))

(defun bind-inet6-socket (socket-fd port &optional (addr "::1"))
  (with-foreign-object (sa 'sockaddr-in6)

    (memzero sa +sockaddr-in6-len+)
;;    (hexdump-foreign sa +sockaddr-in6-len+)

    ;; init struct
    (setf (foreign-slot-value sa 'sockaddr-in6 'len) +sockaddr-in6-len+
	  (foreign-slot-value sa 'sockaddr-in6 'port) (%htons port)
	  (foreign-slot-value sa 'sockaddr-in6 'family) +af-inet6+)

    ;; set addr
    (if (/= (%inet-pton +af-inet6+ addr (foreign-slot-pointer sa 'sockaddr-in6 'addr)) 1)
	(error "inet_pton: Bad address ~A!" addr))

    ;; bind
    (if (= (%bind socket-fd sa +sockaddr-in6-len+) 0)
	t
	nil)))


;;;; SOCKET I/O

(defstruct socket fd 
	   family remote-host remote-port 
	   foreign-read-buffer read-buffer-sz read-used-sz 
	   write-queue)

(defun socket-remote-info (socket)
  (list (socket-family socket) (socket-remote-host socket) (socket-remote-port socket)))

(defstruct write-queue-entry buffer size written)

(defun socket-accept (socket-fd &optional (buffer-size 4096))
  "Accept connection from SOCKET-FD. Allocates and returns socket structure denoting the connection."

  (with-foreign-object (addr 'sockaddr-in6)

    (let ((len (foreign-alloc :unsigned-long :initial-element +sockaddr-in6-len+)))

      ;; accept connection
      (let* ((res (%accept socket-fd addr len)) 
	     (data (make-socket :fd res  
				:foreign-read-buffer (foreign-alloc :unsigned-char :count buffer-size) 
				:read-buffer-sz buffer-size
				:read-used-sz 0)))
	(unless (< res 0)
	  (let ((len-value (mem-ref len :unsigned-int)))

	    ;; parse sockaddr struct for remote client info
	    (if (= len-value +sockaddr-in6-len+)
		(setf (socket-family data) :inet6
		      (socket-remote-port data) (foreign-slot-value addr 'sockaddr-in6 'port)
		      (socket-remote-host data)
		      (let ((client-addr (foreign-slot-value addr 'sockaddr-in6 'addr)))
			(loop for i from 0 to 7 
			   for hex = (mem-aref client-addr :unsigned-short i)
			   collect hex into res
			   finally (return res))))

		(if (= len-value +sockaddr-in-len+)
		    (setf (socket-family data) :inet4
			  (socket-remote-port data) (foreign-slot-value addr 'sockaddr-in 'port)
			  (socket-remote-host data)
			  (let ((client-addr (foreign-slot-value addr 'sockaddr-in 'addr)))
			    (loop for i from 0 to 3
			       for hex = (logand (ash client-addr (- (* i 8))) #xFF)
			       collect hex into res
			       finally (return res))))

		    (setf (socket-family data) :unknown)))))

	(foreign-free len)
	(if (>= res 0)
	    data
	    nil)))))

(defun read-socket (socket)
  (let (new-bytes cur-ptr nbytes)
    (setq cur-ptr (inc-pointer (socket-foreign-read-buffer socket) (socket-read-used-sz socket))
	  nbytes (- (socket-read-buffer-sz socket) (socket-read-used-sz socket))
	  new-bytes (%read (socket-fd socket) cur-ptr nbytes))
;;    (format t "READ-SOCKET: ~D bytes read from fd ~D.~%" new-bytes (socket-fd socket)) (force-output)
    (cond
      ((< new-bytes 0) ;; error
       :error)
;;       (error "Error reading from socket"))  ;;FIXME!! should throw exception instead
      ((= new-bytes 0) ;; eof
;;       (error "EOF"))
       :eof
       )
      (t
       (incf (socket-read-used-sz socket) new-bytes) ;; FIXME !! should handle buffer growing ..
       (map-to-lisp-array (socket-foreign-read-buffer socket) (socket-read-used-sz socket))))))


(defun write-socket (socket data)
  ;; add data to write queue
  (destructuring-bind (array length) (map-to-foreign-array data)
    
    (let ((entry (make-write-queue-entry :buffer array :size length :written 0)))
      (setf (socket-write-queue socket) (append (socket-write-queue socket) (cons entry nil)))))

  (write-more socket))

(defun write-more (socket)
  (let ((fd (socket-fd socket))
	(queue (socket-write-queue socket))
	new-queue)

;;    (format t "WRITE-MORE: for fd ~D, we have queue ~S.~%" fd queue)

    ;; loop for entries in queue

    (loop for elt in queue do
	 (let ((buffer (write-queue-entry-buffer elt))
	       (size (write-queue-entry-size elt))
	       (written (write-queue-entry-written elt))
	       bytes-left now-written)

	   (setq bytes-left (- size written))
	   (setq now-written (%write fd (inc-pointer buffer written) bytes-left))

;;	   (format t "WRITE-MORE: written ~D bytes for buffer ~A.~%" now-written buffer)

	   ;; if no bytes written, return
	   (if (= now-written 0) (return)) 

	   (incf (write-queue-entry-written elt) now-written)))

    ;; clean buffers that are written

    (loop for elt in (socket-write-queue socket) 
       for size = (write-queue-entry-size elt)
       for written = (write-queue-entry-written elt)

       when (= size written)
       do
;;	 (format t "freeing ~A.~%" elt)
	 (foreign-free (write-queue-entry-buffer elt))
	     
       when (< written size)
       do (setq new-queue (append new-queue elt)))

;;    (format t "WRITE-MORE: after cleanup for fd ~D, we have queue ~S.~%" fd new-queue)
    (setf (socket-write-queue socket) new-queue)
    t))
	   
	   
  
(defun test ()
  (let ((sock (make-inet-socket)))
    (unless (bind-inet-socket sock 1235)
      (error "Unable to bind!"))
    (start-listen sock)
    (format t "bind/listen ok.~%")
    (close-socket sock)
    t))

(defun test6 ()
  (let ((sock (make-inet6-socket)))
    (unless (bind-inet6-socket sock 1235)
      (error "Unable to bind!"))
    (start-listen sock)
    (format t "bind/listen ok.~%")
    (close-socket sock)
    t))