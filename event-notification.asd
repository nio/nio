;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package :asdf)

(defsystem :event-notification

    :components ((:file "event-notification")
		 (:file "kqueue-cffi" :depends-on ("event-notification"))
		 (:file "epoll-cffi" :depends-on ("event-notification"))
		 (:file "kqueue" :depends-on ("event-notification" "kqueue-cffi"))
		 (:file "epoll" :depends-on ("event-notification" "epoll-cffi")))

    :depends-on (:cffi))

