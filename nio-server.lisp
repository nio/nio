#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(defpackage :nio-server (:use :cl :nio :event-notification) 
	    (:export start-server))
(in-package :nio-server)

(defun start-server (connection-handler accept-request &key 
		     (protocol :inet) (port (+ (random 60000) 1024)) (host "localhost") 
		     (accept-connection #'(lambda (proto host port) (declare (ignore proto host port)) t)))

  (let ((sock (ecase protocol (:inet (make-inet-socket)) (:inet6 (make-inet6-socket))))
	(event-queue (make-event-queue))
	(sock-hash (make-hash-table :test 'eql))
	)

    (unless (ecase protocol 
	      (:inet (bind-inet-socket sock port host))
	      (:inet6 (bind-inet6-socket sock port host)))
      (error "Can't bind socket!"))

    (set-socket-nonblocking sock)

    (unwind-protect 
	 (progn
	   (format t "Starting server on ~S port ~S.. (socket fd is ~D)~%" host port sock)

	   (start-listen sock)
	   (add-fd event-queue sock)
	   
	   (format t "waiting for events..~%") (force-output)

	   (loop for idents = (poll-events event-queue) do

		(loop for ident in idents do

		     (cond
		       ;; new connection
		       ((= ident sock) 

			  (let ((client (socket-accept ident)))
			    (unless (null client)
			      (destructuring-bind (proto host port) (socket-remote-info client)

				(set-socket-nonblocking (socket-fd client))

				;; accept ?
				(if (funcall accept-connection proto host port)
				    (progn
				      (setf (gethash (socket-fd client) sock-hash) client)
				      (add-fd event-queue (socket-fd client)))

				    ;; no
				    (progn
				      (close-socket (socket-fd client))))))))
				      

		       ;; socket i/o available
		       (t

			(let ((client (gethash ident sock-hash)) data)
			  
;;			  (format t "Client is ~S.~%" client)

			  (setq data (read-socket client))

			  ;; if keyword, error occured (error reading or EOF)
			  (if (keywordp data)
			      (remove-fd event-queue (socket-fd client))

			      ;; no error, test
			      (if (funcall accept-request data)

				  (let ((status (funcall connection-handler client data)))

				    (unless (eql status :keep-alive)
				      (remove-fd event-queue (socket-fd client))
				      (close-socket (socket-fd client))))))))


		       ))))

      (close-socket sock))))