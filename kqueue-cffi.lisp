#|
Copyright (c) 2006 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :event-notification)

#+(or darwin freebsd)
(progn
  (defcfun ("warn" %warn) :void
    (format :string))

  (defcfun ("memset" %memset) :pointer
    (buffer :pointer)
    (byte :int)
    (len :int))

  (defun memzero (ptr size)
    (%memset ptr 0 size))


;;;; KQUEUE FUNCTIONS / STRUCTURES

  (defcfun ("kqueue" %kqueue) :int)

  (defcfun ("kevent" %kevent) :int
    (kq :int)
    (changelist :pointer)
    (n-changes :int)
    (eventlist :pointer)
    (n-events :int)
    (timeout :pointer))

  (defconstant +kevent-size+ #.(+ 4 4 4 4 4 4))

  (defcstruct kevent
      (ident :unsigned-long)
    (filter :short)
    (flags :unsigned-short)
    (fflags :unsigned-int)
    (data :long)
    (udata :pointer))

  (defconstant +evfilt-read+ -1)
  (defconstant +evfilt-write+ -2)
  (defconstant +evfilt-aio+ -3)


  (defconstant +ev-add+ 1)
  (defconstant +ev-delete+ 2)

  (defconstant +ev-clear+ #x20)

  )