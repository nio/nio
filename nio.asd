;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package :asdf)

(defsystem :nio

    :components ((:file "nio")
		 (:file "socket-cffi" :depends-on ("nio"))
		 (:file "socket" :depends-on ("nio" "socket-cffi")))

    :depends-on (:cffi))

